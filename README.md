# .NET 6.0 Docker Example
A simple demonstration containerizing a C# ASP.NET Core Web API project using Docker. Note the Dockerfile - the contents for which were sourced from https://learn.microsoft.com/en-us/dotnet/core/docker/build-container and moddified for a .NET 6.0 project.
Also note that security has been integrated into this API project - see Program.cs and WeatherForecastController.cs, and explore the user secrets.json file.
A demo from the Sweden 2023 full stack bootcamp.


## Installation

To clone this repository, run the following command:

```bash
git clone https://gitlab.com/teacher.warren.english/dotnet6-docker-weatherforecast-example.git
```

## Contributing

Warren West (@teacher.warren.english)

## License
©️ Noroff Accelerate
[MIT](https://choosealicense.com/licenses/mit/)
